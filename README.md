This folder contains:

- A file 'TFS_diagrams.pdf' that contains details on Trait-Function-Service diagrams for 3 services.
- A file 'expert_rankings.pdf' that contains a table that provide for each reference (16 in total), a ranking of the species. The rankings of each reference are used to set up a the expert ranking. 
- Other files that describe the ontology (in English) and rules (in French).
