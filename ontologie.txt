%% BASIC ONTOLOGY
%% CONCEPTS 
trait	% functional trait
ecosystemProperty
	function % ecosystem function
	service % ecosystem service
        biophysicalServiceOfAgriculturalProduction
        biophysicalServiceOfTheLivingEnvironment
        biologicalService
species
source
  dataSource
  userSource
growingCondition
aggregation

%% RULES FOR HIERARCHY OF CONCEPTS
top(T):-trait(T).
top(X):-ecosystemProperty(X).
ecosystemProperty(F):-function(F).
ecosystemProperty(S):-service(S).
service(S):-biophysicalServiceOfAgriculturalProduction(S).
service(S):-biophysicalServiceOfTheLivingEnvironment(S).
service(S):-biologicalService(S).
top(E):-species(E).
entite(S):-source(S).
source(S):-dataSource(S).
source(S):-userSource(S).
top(X):-growingCondition(X).
top(A):-aggregation(A).


%% USERS RELATIONS
%% BINARY RELATIONS (+ HIERARCHY)
isLinkedToFunction(Trait,Function)
	isPositivelyLinkedToFunction(Trait,Function)
	isNegativelyLinkedToFunction(Trait,Function)

isLinkedToService(Function,Service)
	isPositivelyLinkedToService(Function,Service)
	isNegativelyLinkedToService(Function,Service)

hasAggregationMethod(EcosystemProperty, Aggregation)

%% SIGNATURES
trait(Trait):-isLinkedToFunction(Trait,Function).
function(Function):-isLinkedToFunction(Trait,Function).

function(Function):-isLinkedToService(Function,Service).
service(Service):-isLinkedToService(Function,Service).

ecosystemProperty(EcosystemProperty):-hasAggregationMethod(EcosystemProperty, Aggregation).
aggregation(Aggregation):-hasAggregationMethod(EcosystemProperty, Aggregation).

% TERNARY RELATIONS
isWeightedLinkedToFunction(Trait,Function,Weight)
isWeightedLinkedToService(Function,Service,Weight)
hasMatchingTraitID(Trait,ID,DataSource).
hasGlobalPriority(DataSource1, DataSource2).
hasPriorityForTrait(Trait, DataSource1, DataSource2).

% SIGNATURES
trait(Trait):-isWeightedLinkedToFunction(Trait,Function,Int).
function(Function):-isWeightedLinkedToFunction(Trait,Function,Int).

function(Function):-isWeightedLinkedToService(Function,Service,Int).
service(Service):-isWeightedLinkedToService(Function,Service,Int).

trait(Trait):-hasMatchingTraitID(Trait,ID,DataSource).
dataSource(DataSource):-hasMatchingTraitID(Trait,ID,DataSource).

dataSource(DataSource1):-hasGlobalPriority(DataSource1, DataSource2).
dataSource(DataSource2):-hasGlobalPriority(DataSource1, DataSource2).

trait(Trait):-hasPriorityForTrait(Trait, DataSource1, DataSource2).
dataSource(DataSource1):-hasPriorityForTrait(Trait, DataSource1, DataSource2).
dataSource(DataSource2):-hasPriorityForTrait(Trait, DataSource1, DataSource2).

% OTHER ARITY RELATIONS
hasSingleDBTraitValue(Trait,Species,GrowingCondition,Value,Source)
hasTraitValue(Trait,Species,GrowingCondition,Value,Source)
hasFunctionValue(Function,Species,Value,Reliability,GrowingCondition)
hasUserValueOfFunction(Function,Species,Value,Reliability,GrowingCondition)
hasComputedValueOfFunction(Function,Species,Value,Reliability,GrowingCondition)
hasServiceValue(Service,Species,Value,Reliability,GrowingCondition)

% SIGNATURES
trait(Trait):-hasSingleDBTraitValue(Trait,Species,GrowingCondition,Value,Source).
species(Species):-hasSingleDBTraitValue(Trait,Species,GrowingCondition,Value,Source).
growingCondition(GrowingCondition):-hasSingleDBTraitValue(Trait,Species,GrowingCondition,Value,Source).
source(Source):-hasSingleDBTraitValue(Trait,Species,GrowingCondition,Value,Source).

trait(Trait):-hasTraitValue(Trait,Species,GrowingCondition,Value,Source).
species(Species):-hasTraitValue(Trait,Species,GrowingCondition,Value,Source).
growingCondition(GrowingCondition):-hasTraitValue(Trait,Species,GrowingCondition,Value,Source).
source(Source):-hasTraitValue(Trait,Species,GrowingCondition,Value,Source).

function(Function):-hasFunctionValue(Function,Species,Value,Reliability,GrowingCondition).
species(Species):-hasFunctionValue(Function,Species,Value,Reliability,GrowingCondition).
growingCondition:-hasFunctionValue(Function,Species,Value,Reliability,GrowingCondition).

function(Function):-hasUserValueOfFunction(Function,Species,Value,Reliability,GrowingCondition).
species(Species):-hasUserValueOfFunction(Function,Species,Value,Reliability,GrowingCondition).
growingCondition(GrowingCondition):-hasUserValueOfFunction(Function,Species,Value,Reliability,GrowingCondition).

function(Function):-hasComputedValueOfFunction(Function,Species,Value,Reliability,GrowingCondition).
species(Species):-hasComputedValueOfFunction(Function,Species,Value,Reliability,GrowingCondition).
growingCondition(GrowingCondition):-hasComputedValueOfFunction(Function,Species,Value,Reliability,GrowingCondition).

service(Service):-hasServiceValue(Service,Species,Value,Reliability,GrowingCondition).
species(Species):-hasServiceValue(Service,Species,Value,Reliability,GrowingCondition).
growingCondition(GrowingCondition):-hasServiceValue(Service,Species,Value,Reliability,GrowingCondition).



